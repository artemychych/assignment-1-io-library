section .data
    nl_chr: equ 10
    spc_chr: equ 32
    tab_chr: equ 9
    null_chr: equ 0
    newline:    db 10
    test: db 'ashdb asdhabs dahb', 0
    print_uint_buffer:    db 100 DUP(0)
section .text
    
 
; Принимает код возврата и завершает текущий процесс
exit:
	mov   	 rax, 60   		 ; exit code to rax register
	syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	mov    rax, 0   		 ; length counter
    
	cmp byte [rdi], 0
	je .done   			 ; if stroke is \0 goto .done
    
	.loop:
   	 inc rax
   	 cmp byte [rdi + rax], 0   	 ; check its \0si
   	 
   	 jne .loop   		 ; if not 0 return to .loop
    
	.done:
   	 ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	mov r12, rdi
	call string_length
	mov rsi, r12
	xor r12, r12
	mov rdx, rax
	mov rdi, 1
	mov rax, 1
	syscall
    
	ret
   	 

; Принимает код символа и выводит его в stdout
print_char:
	push rdi
	mov rdx, 1
	mov rax, 1
	mov rsi, rsp
	mov rdi, 1
	syscall
	pop rdi
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, 1
	mov rsi, newline
	mov rdx, 1
	mov rax, 1

	ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov rax, rdi
	mov r10, 10
	mov rsi, print_uint_buffer
	push 0
    
	.loop:
   	 xor rdx, rdx
   	 div r10
   	 add rdx, '0'
   	 push rdx
   	 cmp rax, 0
   	 ja .loop
   	 
	.write_buffer:
   	 pop rdi
   	 cmp rdi, 0
   	 jz .done
   	 mov [rsi], rdi
   	 inc rsi
   	 jmp .write_buffer
   	 
	.done:
   	 mov byte [rsi], 0
   	 mov rdi, print_uint_buffer
   	 call print_string
   	 ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
	cmp rdi, 0
	jge print_uint
	neg rdi
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	call print_uint
	ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	mov r11, 0
	mov r12, 0
    
	.loop:
   	 mov r11b, byte [rdi]
   	 mov r12b, byte [rsi]
   	 
   	 cmp r11, r12
   	 jne .not_equal

   	 inc rdi
   	 inc rsi
   	 
   	 cmp r11, 0
   	 je .done
   	 
   	 jmp .loop
   	 
	.not_equal:
  	 mov rax, 0
  	 xor r12, r12
  	 xor r11, r11
  	 ret
  	 
	.done:
   	 mov rax, 1
   	 xor r12, r12
   	 xor r11, r11
    	ret
    
; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	mov rax, 0
	mov rdi, 0
	push 0
	mov rsi, rsp
	mov rdx, 1
	syscall
	pop rax
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:  
	mov r8, rdi
	mov r9, rsi
	mov r10, rdi
	mov r11, 0
    
	.loop:
   	 cmp r11, r9
   	 je .tozero
   	 
   	 push r11
   	 push r10
   	 push r9
   	 push r8
   	 call read_char
   	 pop r8
   	 pop r9
   	 pop r10
   	 pop r11
   	 
   	 cmp al, null_chr
   	 je .end
   	 
   	 cmp al, tab_chr
   	 je .check
   	 
   	 cmp al, nl_chr
   	 je .check
   	 
   	 cmp al, spc_chr
   	 je .check
   	 
   	 mov [r8], al
   	 
   	 inc r8
   	 inc r11
   	 jmp .loop
   	 
	.tozero:
   	 mov rax, 0
   	 ret

	.check:
   	 inc r8
   	 cmp r11, 0
   	 je .loop

	.end:
   	 mov byte [r8], 0x0
   	 mov rax, r10
   	 mov rdx, r11
   	 ret


 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor rax, rax
	xor r8, r8
	xor r9, r9
	mov r10, 10
    
	.loop:
   	 cmp byte [rdi], '0'
   	 jb .done
   	 
   	 cmp byte [rdi], '9'
   	 ja .done
   	 
   	 mul r10
   	 mov r8b, byte [rdi]
   	 sub r8b, '0'
   	 add rax, r8
   	 inc rdi
   	 inc r9
   	 jmp .loop
   	 
	.done:
   	 mov rdx, r9
   	 ret    




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], '-'
    je .neg
    jmp parse_uint
    
    .neg:
   	 inc rdi
   	 call parse_uint
   	 cmp rdx, 0
   	 je .end_error
   	 
    .end_done:
   		 inc rdx
   	 neg rax
   	 ret    
   	 
    .end_error:
   	 ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	push rdi
	push rsi
	call string_length
	pop rsi
	pop rdi
	cmp rdx, rax
	jle .error
    
	.loop:
   	 mov rdx, [rdi]
   	 mov [rsi], rdx
   	 
   	 cmp byte [rdi], 0
   	 je .end
   	 
   	 inc rdi
   	 inc rsi
   	 jmp .loop
   	 
	.error:
   	 mov rax, 0
   	 
	.end:
   	 ret

